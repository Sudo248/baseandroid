package com.sudo248.baseandroid

import com.sudo248.base_android.app.BaseApplication


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 11:43 - 27/02/2023
 */
class App : BaseApplication() {
    override val enableSupportLanguages: Boolean = true
}