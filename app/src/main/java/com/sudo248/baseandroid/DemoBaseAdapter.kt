package com.sudo248.baseandroid

import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.base_android.base.BaseViewHolder
import com.sudo248.baseandroid.databinding.ItemBinding

class DemoBaseAdapter : BaseListAdapter<String, DemoBaseAdapter.DemoItemViewHolder>() {

    override val enableLoadMore: Boolean = true

    class DemoItemViewHolder(binding: ItemBinding) : BaseViewHolder<String, ItemBinding>(binding) {
        override fun onBind(item: String) {
            binding.txt.text = item
        }
    }
}