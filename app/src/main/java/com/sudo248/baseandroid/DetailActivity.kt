package com.sudo248.baseandroid

import android.os.Bundle
import com.sudo248.base_android.base.BaseActivity
import com.sudo248.baseandroid.databinding.ActivityDetailBinding

class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>() {
    override val viewModel: DetailViewModel = DetailViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.btnBack.setOnClickListener {
            viewModel.back()
        }
    }

    override fun initView() {

    }
}