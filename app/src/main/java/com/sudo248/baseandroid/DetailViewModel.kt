package com.sudo248.baseandroid

import android.os.Bundle
import com.sudo248.base_android.base.BaseViewModel
import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.navigation.IntentDirections
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.measureTime
import kotlin.time.measureTimedValue


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 21:09 - 02/03/2023
 */
class DetailViewModel : BaseViewModel<IntentDirections>() {

    fun back() {
        val bundle = Bundle().apply {
            putString("duong", "Ngo Thi Kieu Oanh")
        }
        navigator.back("", bundle)
    }
}