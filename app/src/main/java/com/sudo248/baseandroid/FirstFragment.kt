package com.sudo248.baseandroid

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.base.EmptyViewModel
import com.sudo248.base_android.navigation.ResultCallback
import com.sudo248.baseandroid.databinding.FragmentFirstBinding

/**
 * A simple [Fragment] subclass.
 * Use the [FirstFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FirstFragment() : BaseFragment<FragmentFirstBinding, EmptyViewModel<NavDirections>>() {
    override val viewModel: EmptyViewModel<NavDirections> by viewModels()

    var text = ""

    override fun initView() {
        binding.txtResult.text = text
        Log.d("Sudoo", "initView: ")
        binding.btnNavigate.setOnClickListener {
            navigateForResult(
                FirstFragmentDirections.actionFirstFragmentToSecondFragment(),
                "Sudoo",
                object : ResultCallback {
                    override fun onResult(key: String, data: Bundle?) {
                        val result = data?.getString("Sudoo") ?: "Empty"
                        text = result
                        Log.d("Sudoo", "onResult: ")
                    }
                })
        }
    }




}