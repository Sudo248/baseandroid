package com.sudo248.baseandroid

import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.sudo248.base_android.base.BaseActivity
import com.sudo248.base_android.base.LoadMoreRecyclerViewListener
import com.sudo248.baseandroid.databinding.ActivityMainBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    override val viewModel: MainViewModel = MainViewModel()

    override fun initView() {
        val adapter = DemoBaseAdapter()
        binding.rcv.adapter = adapter
//        adapter.submitList(getData(10))
        viewModel.get1()
        lifecycleScope.launch {
            delay(1000)
            adapter.submitList(getData(10))
        }
        adapter.setLoadMoreListener(object : LoadMoreRecyclerViewListener {
            override fun onLoadMore(page: Int, itemCount: Int) {
                Log.d("Sudoo", "onLoadMore -> page: $page itemCount: $itemCount")
                lifecycleScope.launch {
                    delay(2000)
                    adapter.submitData(getData(10, itemCount), extend = true)
                }
            }
        })
//        binding.rcv.setHorizontalViewPort(3.5f)
    }

    fun getData(limit: Int, last: Int = 0): List<String> {
        return List(limit) {
            "Item ${last+it}"
        }
    }
}