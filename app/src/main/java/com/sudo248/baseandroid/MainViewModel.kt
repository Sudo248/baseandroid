package com.sudo248.baseandroid

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.sudo248.base_android.base.BaseViewModel
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.ktx.createActionIntentDirections
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.base_android.navigation.ResultCallback
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 10:01 - 23/02/2023
 */
class MainViewModel : BaseViewModel<IntentDirections>() {

    val demo = MutableStateFlow(UiState.IDLE)
    val demo1 = MutableLiveData(1)

    fun publish(s: String) {
//        SudoBus.get().publish("duong", s)
    }

    fun get1() {
        launch {
            setState(UiState.LOADING)
//            delay(2000)
//            setState(StateScreen.RESUME)
        }
//        launch {
//            demo.emit(StateScreen.LOADING)
//            delay(1000)
//            demo.value = StateScreen.RESUME
//        }
    }

    fun navigate() {
        navigator.navigateForResult(DetailActivity::class.createActionIntentDirections(), "", object : ResultCallback {
            override fun onResult(key: String, data: Bundle?) {
                val value = data?.getString("duong")
                Log.d("Sudoo", "onResult: $value")
            }
        })
    }
}