package com.sudo248.baseandroid

import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.base.EmptyViewModel
import com.sudo248.baseandroid.databinding.FragmentSecondBinding


class SecondFragment : BaseFragment<FragmentSecondBinding, EmptyViewModel<NavDirections>>() {
    override val viewModel: EmptyViewModel<NavDirections> by viewModels()

    override fun initView() {
        binding.btnBack.setOnClickListener {
            back("Sudoo", bundleOf(Pair("Sudoo", binding.txtResult.text.toString())))
        }
    }


}