package com.sudo248.baseandroid

import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.baseandroid.databinding.ItemBinding

class StringAdapter : ListAdapter<String, StringAdapter.StringItem>(
    object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

    }
) {

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        recyclerView.offsetChildrenHorizontal(100)
        val width = MeasureSpec.makeMeasureSpec(recyclerView.width, MeasureSpec.EXACTLY)

    }

    class StringItem(private val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(value: String) {
            binding.txt.text = value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringItem {
        return StringItem(
            ItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StringItem, position: Int) {
        holder.onBind(getItem(position))
    }

}