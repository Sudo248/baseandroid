package com.sudo248.baseandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class TestFragmentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_fragment)
    }
}