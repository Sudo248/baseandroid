package com.sudo248.baseandroid.language

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.LocaleList
import com.sudo248.base_android.base.BaseActivity
import com.sudo248.base_android.base.EmptyViewModel
import com.sudo248.base_android.base.locale.LocaleManager
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.baseandroid.R
import com.sudo248.baseandroid.databinding.ActivityLanguageBinding

class LanguageActivity : BaseActivity<ActivityLanguageBinding, EmptyViewModel.Activity>() {
    override val viewModel: EmptyViewModel.Activity
        get() = EmptyViewModel.Activity()

    override fun initView() {
        if (LocaleManager.getCurrentLanguageCode() == "vi") {
            binding.rbVi.isChecked = true
        } else {
            binding.rbEn.isChecked = true
        }

        binding.rbVi.setOnCheckedChangeListener { _, b ->
            if (b) {
                LocaleManager.of(this).applyLocalized("vi")
            }
        }

        binding.rbEn.setOnCheckedChangeListener { _, b ->
            if (b) {
                LocaleManager.of(this).applyLocalized("en")
            }
        }
    }
}