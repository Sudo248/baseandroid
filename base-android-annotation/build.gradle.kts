import plugins.`publish-kotlin-module`

plugins {
    `publish-kotlin-module`
    id("org.jetbrains.kotlin.jvm")
}

description = "Base Android annotation library"
