package com.sudo248.base_android_annotation.apiservice


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 08:24 - 25/02/2023
 */

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiService(val baseUrl: String)
