package com.sudo248.base_android_annotation.apiservice


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 15:48 - 04/03/2023
 */

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class AuthenticationRequired()
