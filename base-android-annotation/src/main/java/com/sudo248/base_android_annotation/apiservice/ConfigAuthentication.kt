package com.sudo248.base_android_annotation.apiservice


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 15:43 - 04/03/2023
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class ConfigAuthentication(
    val token: String,
    val type: String = "Bearer",
    val headerName: String = "Authorization",
)
