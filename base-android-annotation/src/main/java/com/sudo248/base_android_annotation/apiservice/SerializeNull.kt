package com.sudo248.base_android_annotation.apiservice

/**
 * By default [Gson] will ignore field that is null
 *
 * This annotation allow to send field [null] in body of request
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class SerializeNull()
