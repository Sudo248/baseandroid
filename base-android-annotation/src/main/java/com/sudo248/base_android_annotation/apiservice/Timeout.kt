package com.sudo248.base_android_annotation.apiservice


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 09:04 - 25/02/2023
 */

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class Timeout(val read: Long = 10_000L, val write: Long = 10_000L)
