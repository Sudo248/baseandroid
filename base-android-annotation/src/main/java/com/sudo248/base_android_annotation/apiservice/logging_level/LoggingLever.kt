package com.sudo248.base_android_annotation.apiservice.logging_level


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 08:29 - 25/02/2023
 */

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class LoggingLever(val level: Level = Level.AUTO)
