import dependencies.Dependencies

plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
    id("kotlin-kapt")
}

java {
    withJavadocJar()
    withSourcesJar()
    sourceCompatibility = Configs.javaVersion
    targetCompatibility = Configs.javaVersion
}

dependencies {
    // Code generation library for kotlin
    implementation(Dependencies.Processor.kotlinPoet)

    // configuration generator for service providers
    implementation(Dependencies.Processor.autoService)
    kapt(Dependencies.Processor.autoService)

//    implementation(Dependencies.Base.annotation)
}


