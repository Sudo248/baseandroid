import dependencies.Dependencies
import dependencies.GroupDependencies
import plugins.`publish-android-module`

plugins {
    `publish-android-module`
    id("androidx.navigation.safeargs")
    id("kotlin-kapt")
}

description = "Base Android library"

android {
    resourcePrefix("base_android")
    buildFeatures {
        dataBinding = true
    }
}

dependencies {
    debugApi(project(Dependencies.Project.baseAndroidAnnotation))
    api(Dependencies.Base.annotation)

    GroupDependencies.androids.forEach {
        implementation(it)
    }
    
    GroupDependencies.retrofits.forEach {
        implementation(it)
    }

    implementation(Dependencies.Kotlin.serialization)

    GroupDependencies.unitTests.forEach {
        testImplementation(it)
        androidTestImplementation(it)
    }

    androidTestImplementation(Dependencies.UnitTest.robolectric)
    androidTestImplementation(Dependencies.UnitTest.junitExt)

    implementation(Dependencies.Android.coroutineCore)
}