package com.sudo.base_android.connectivity

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sudo248.base_android.connectivity.ConnectivityObserver
import com.sudo248.base_android.connectivity.NetworkConnectivityObserver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowNetworkCapabilities

@RunWith(AndroidJUnit4::class)
class NetworkConnectivityObserverTest {

    lateinit var context: Context
    @OptIn(ExperimentalCoroutinesApi::class)
    val testDispatcher: TestDispatcher = StandardTestDispatcher(name = "TestMain")
    lateinit var connectivityObserver: ConnectivityObserver

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun initData() {
        context = Mockito.mock(Context::class.java)
        connectivityObserver = NetworkConnectivityObserver(context)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun NetworkConnectivity_isTrue() {
        runBlocking {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val networkCapabilities = ShadowNetworkCapabilities.newInstance()
            shadowOf(networkCapabilities).addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            shadowOf(connectivityManager).setNetworkCapabilities(connectivityManager.activeNetwork, networkCapabilities)

            launch {
                connectivityObserver.observer().collect {
                    Assert.assertTrue(it.hasNetwork())
                }
            }
        }
    }



}