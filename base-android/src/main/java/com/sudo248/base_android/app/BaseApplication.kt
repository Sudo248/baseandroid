package com.sudo248.base_android.app

import android.app.Application
import com.sudo248.base_android.base.locale.LocaleManager
import com.sudo248.base_android.core.bus.SudoBus
import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.base_android.utils.NetworkUtils
import com.sudo248.base_android.utils.ViewUtils

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

open class BaseApplication : Application() {
    private var sudoBus: SudoBus? = null
    protected open val enableNetworkObserver: Boolean = false
    open val enableSupportLanguages: Boolean = false

    override fun onCreate() {
        super.onCreate()
        installInIfNeeded()
    }

    private fun installInIfNeeded() {
        if (enableNetworkObserver) NetworkUtils.installIn(this)
        AppSharedPreference.create(this)
        ViewUtils.getDefaultLoadingView(this)
        LocaleManager.setEnableSupportLanguage(enableSupportLanguages)
    }

    fun getSudoBus(): SudoBus {
        return sudoBus ?: SudoBus.createInstance().also { sudoBus = it }
    }
}