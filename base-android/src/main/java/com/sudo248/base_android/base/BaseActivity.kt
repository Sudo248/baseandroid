package com.sudo248.base_android.base

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.LayoutRes
import androidx.annotation.StyleRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewbinding.ViewBinding
import com.sudo248.base_android.app.BaseApplication
import com.sudo248.base_android.base.locale.LocaleManager
import com.sudo248.base_android.ktx.hideKeyBoard
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.base_android.navigation.Navigator
import com.sudo248.base_android.navigation.ResultCallback
import com.sudo248.base_android.utils.BindingReflex
import com.sudo248.base_android.utils.ViewUtils
import kotlinx.coroutines.launch

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

abstract class BaseActivity<out VB : ViewBinding, out VM : BaseViewModel<IntentDirections>> :
    AppCompatActivity(),
    Navigator<IntentDirections>,
    BaseViewController {

    private var _binding: VB? = null
    protected val binding: VB get() = _binding!!

    protected abstract val viewModel: VM

    @LayoutRes
    protected open val layoutId = -1

    @LayoutRes
    protected open val layoutLoadingId: Int? = null

    private var loadingDialog: AlertDialog? = null

    protected open val isAutoClearFocus: Boolean = true

    private var resultCallback: ResultCallback? = null

    private val startActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            val data = it.data?.extras
            resultCallback?.onResult(it.resultCode.toString(), data)
            resultCallback = null
        }

    private fun createBinding(): VB {
        return if (layoutId == -1) {
            BindingReflex.reflexViewBinding(javaClass, layoutInflater)
        } else {
            try {
                DataBindingUtil.inflate(layoutInflater, layoutId, null, false)
            } catch (e: Exception) {
                BindingReflex.reflexViewBinding<VB>(javaClass, layoutInflater)
            }
        }
    }

    protected fun setVariable(variableId: Int, value: Any): Boolean {
        return if (binding is ViewDataBinding) {
            (binding as ViewDataBinding).setVariable(variableId, value)
        } else false
    }

    override fun attachBaseContext(newBase: Context) {
        if (LocaleManager.enableSupportLanguage) {
            val newContext = LocaleManager.of(newBase).getLocalizedContext()
            super.attachBaseContext(newContext)
        } else {
            super.attachBaseContext(newBase)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = createBinding()
        setContentView(_binding!!.root)
        setupBindingLifeCycle()
        initView()
        setupUiState()
        setupNavigator()
        setupViewController()
        observer()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        loadingDialog = null
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (isAutoClearFocus) {
            ev?.let {
                if (ev.action == MotionEvent.ACTION_DOWN) {
                    currentFocus?.let { viewFocus ->
                        if (viewFocus is EditText) {
                            val outRect = Rect()
                            viewFocus.getGlobalVisibleRect(outRect)
                            if (!outRect.contains(it.rawX.toInt(), it.rawY.toInt())) {
                                viewFocus.clearFocus()
                                hideKeyBoard()
                            }
                        }
                    }
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun navigateTo(directions: IntentDirections) {
        val intent = directions.intent
        intent.setClass(this, directions.destination.java)
        startActivity(intent)
    }

    override fun navigateOff(directions: IntentDirections) {
        navigateTo(directions)
        finish()
    }

    override fun navigateOffAll(directions: IntentDirections) {
        val intent = directions.intent
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun navigateForResult(
        directions: IntentDirections,
        key: String,
        result: ResultCallback
    ) {
        val intent = directions.intent
        intent.setClass(this, directions.destination.java)
        resultCallback = result
        startActivityResult.launch(intent)
    }

    override fun back(key: String?, data: Bundle?) {
        if (data != null) {
            val intent = Intent()
            intent.putExtras(data)
            setResult(RESULT_OK, intent)
        }
        finish()
    }

    private fun setupNavigator() {
        viewModel.setupNavigator(this)
    }

    private fun setupViewController() {
        viewModel.setViewController(this)
    }

    private fun setupUiState() {
        collectUiState()
    }

    private fun collectUiState() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    state.getValueIfNotHandled()?.let {
                        it.onState(
                            idle = { onStateIdle() },
                            success = { onStateSuccess() },
                            loading = { onStateLoading() },
                            error = { onStateError(viewModel.error.getValueIfNotHandled()) }
                        )
                    }
                }
            }
        }
    }

    private fun setupBindingLifeCycle() {
        if (binding is ViewDataBinding) {
            (binding as ViewDataBinding).lifecycleOwner = this
        }
    }

    abstract fun initView()

    open fun observer() {}

    open fun onStateIdle() {}

    open fun onStateSuccess() {
        loadingDialog?.dismiss()
    }

    open fun onStateLoading() {
        if (loadingDialog == null) {
            loadingDialog = AlertDialog.Builder(
                this,
                com.sudo248.base_android.R.style.base_android_DefaultLoadingStyle
            )
                .setCancelable(false)
                .setView(
                    layoutLoadingId
                        ?: com.sudo248.base_android.R.layout.base_android_dialog_default_loading
                )
                .show()
        } else if (loadingDialog?.isShowing == false) {
            loadingDialog?.show()
        }
    }

    open fun onStateError(error: Exception?) {
        loadingDialog?.dismiss()
    }

    companion object {}
}