package com.sudo248.base_android.base

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

class BaseDiffCallback<T : Any> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return if (oldItem is ItemDiff) {
            oldItem.isItemTheSame(newItem as ItemDiff)
        } else {
            oldItem == newItem
        }
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return if (oldItem is ItemDiff) {
            oldItem.isContentTheSame(newItem as ItemDiff)
        } else {
            oldItem == newItem
        }
    }
}
