package com.sudo248.base_android.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.clearFragmentResultListener
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDirections
import androidx.navigation.NavOptionsBuilder
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.viewbinding.ViewBinding
import com.sudo248.base_android.navigation.Navigator
import com.sudo248.base_android.navigation.ResultCallback
import com.sudo248.base_android.utils.BindingReflex
import kotlinx.coroutines.launch

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

abstract class BaseFragment<VB : ViewBinding, out VM : BaseViewModel<NavDirections>> : Fragment(),
    Navigator<NavDirections>,
    BaseViewController {
    private var _binding: VB? = null
    protected val binding: VB get() = _binding!!

    protected abstract val viewModel: VM

    @LayoutRes
    protected open val layoutId = -1

    @LayoutRes
    protected open val layoutLoadingId: Int? = null

    protected open val enableOnBackPressed: Boolean = false

    protected open val enableAnimationFragment: Boolean = false

    private fun createBinding(inflater: LayoutInflater, container: ViewGroup?): VB {
        return if (layoutId == -1) {
            BindingReflex.reflexViewBinding(javaClass, inflater)
        } else {
            try {
                DataBindingUtil.inflate(inflater, layoutId, container, false)
            } catch (e: Exception) {
                BindingReflex.reflexViewBinding<VB>(javaClass, inflater)
            }
        }
    }

    private var loadingDialog: AlertDialog? = null

    protected fun setVariable(variableId: Int, value: Any): Boolean {
        return if (binding is ViewDataBinding) {
            (binding as ViewDataBinding).setVariable(variableId, value)
        } else false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = createBinding(inflater = inflater, container = container)
        setupBindingLifeCycle()
        initView()
        setupUiState()
        setupNavigator()
        setupViewController()
        setupOnBackPressed()
        observer()
        return _binding!!.root
    }

    override fun onDestroyView() {
        _binding = null
        loadingDialog = null
        super.onDestroyView()
    }

    override fun navigateTo(directions: NavDirections) {
        findNavController().navigate(
            directions,
            navOptions = navOptions { navOptionsBuilder(this) }
        )
    }

    override fun navigateOff(directions: NavDirections) {
        findNavController().run {
            navigate(
                directions,
                navOptions = navOptions {
                    navOptionsBuilder(this)
                    popUpTo(currentDestination?.id ?: 0) {
                        inclusive = true
                    }
                }
            )
        }
    }

    override fun navigateOffAll(directions: NavDirections) {
        findNavController().run {
            navigate(
                directions,
                navOptions = navOptions {
                    navOptionsBuilder(this)
                    popUpTo(graph.id) {
                        inclusive = true
                    }
                }
            )
        }
    }

    override fun navigateForResult(
        directions: NavDirections,
        key: String,
        result: ResultCallback
    ) {
        navigateTo(directions)
        setFragmentResultListener(key) { _, bundle ->
            clearFragmentResultListener(key)
            result.onResult(key, bundle)
        }
    }

    override fun back(key: String?, data: Bundle?) {
        findNavController().navigateUp()
        if (data != null) {
            require(key is String) { "Key result in fragment required String not null" }
            setFragmentResult(key, data)
        }
    }

    private fun setupOnBackPressed() {
        if (enableOnBackPressed) {
            activity?.onBackPressedDispatcher?.addCallback(
                viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
                            onBackPressed()
                        }
                    }
                })
        }
    }

    private fun setupBindingLifeCycle() {
        _binding?.let {
            if (it is ViewDataBinding) {
                (it as ViewDataBinding).lifecycleOwner = this
            }
        }
    }

    private fun setupUiState() {
        collectUiState()
    }

    private fun setupNavigator() {
        viewModel.setupNavigator(this)
    }

    private fun setupViewController() {
        viewModel.setViewController(this)
    }

    private fun collectUiState() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    state.getValueIfNotHandled()?.let {
                        it.onState(
                            idle = { onStateIdle() },
                            success = { onStateSuccess() },
                            loading = { onStateLoading() },
                            error = { onStateError(viewModel.error.getValueIfNotHandled()) }
                        )
                    }
                }
            }
        }
    }

    private fun defaultNavOptionsBuilder(builder: NavOptionsBuilder) {
        if (enableAnimationFragment) {
            builder.anim {
                enter = android.R.anim.slide_in_left
                exit = android.R.anim.slide_out_right
            }
        }
    }

    open fun initData() {}

    abstract fun initView()

    open fun observer() {}

    open fun onStateIdle() {}

    open fun navOptionsBuilder(builder: NavOptionsBuilder) {
        defaultNavOptionsBuilder(builder)
    }

    open fun onStateSuccess() {
        loadingDialog?.dismiss()
    }

    open fun onStateLoading() {
        if (loadingDialog == null) {
            loadingDialog = AlertDialog.Builder(
                requireContext(),
                com.sudo248.base_android.R.style.base_android_DefaultLoadingStyle
            )
                .setCancelable(false)
                .setView(
                    layoutLoadingId
                        ?: com.sudo248.base_android.R.layout.base_android_dialog_default_loading
                )
                .show()
        } else if (loadingDialog?.isShowing == false) {
            loadingDialog?.show()
        }
    }

    open fun onStateError(error: Exception?) {
        loadingDialog?.dismiss()
    }

    open fun onBackPressed() {
        findNavController().navigateUp()
    }
}