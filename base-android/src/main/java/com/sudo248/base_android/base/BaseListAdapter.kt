package com.sudo248.base_android.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.viewbinding.ViewBinding
import com.sudo248.base_android.R
import com.sudo248.base_android.ui.component.ItemLoadingViewHolder
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 09:55 - 15/10/2022
 */

abstract class BaseListAdapter<T : Any, VH : BaseViewHolder<T, *>> :
    ListAdapter<T, BaseViewHolder<T, *>>(
        BaseDiffCallback<T>()
    ) {

    companion object {
        const val TYPE_VIEW_LOADING = -1
    }

    open val enableLoadMore: Boolean = false

    @LayoutRes
    protected open val layoutLoading: Int = R.layout.base_android_item_loading

    private var defaultVHClass: Class<BaseViewHolder<T, *>>? = null

    private var inflaterViewBinding: Method? = null

    private var loadMoreRecyclerViewListener: LoadMoreRecyclerViewListener? = null

    protected val thresholdInvisibleItem: Int? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        if (enableLoadMore) {
            recyclerView.addOnScrollListener(
                object : LoadMoreRecyclerViewScrollListener(
                    recyclerView.layoutManager!!,
                    thresholdInvisibleItem ?: 0
                ) {
                    override fun onLoadMore(page: Int, itemCount: Int) {
                        loadMoreRecyclerViewListener?.onLoadMore(page, itemCount)
                    }
                })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T, *> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return if (viewType == TYPE_VIEW_LOADING) {
            ItemLoadingViewHolder(
                layoutInflater.inflate(layoutLoading, parent, false),
                getRecyclerViewOrientation(parent as RecyclerView)
            )
        } else {
            getViewHolder(layoutInflater, parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T, *>, position: Int) {
        if (holder.itemViewType != TYPE_VIEW_LOADING) {
            holder.onBind(getItem(position))
        }
    }

    open fun submitData(list: List<T>?, extend: Boolean = false) {
        if (extend) {
            val newList = currentList.toMutableList()
            newList.addAll(list ?: emptyList())
            super.submitList(newList)
        } else {
            super.submitList(list?.toList())
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (enableLoadMore && isLastPosition(position)) {
            TYPE_VIEW_LOADING
        } else {
            getViewType(position)
        }
    }

    override fun getItemCount(): Int =
        if (enableLoadMore) currentList.size + 1 else currentList.size

    private fun isLastPosition(position: Int): Boolean = position >= itemCount - 1

    private fun getMethodInflateViewBinding(vhClass: Class<BaseViewHolder<T, *>>): Method {
        val viewBindingClass =
            (vhClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<ViewBinding>
        return viewBindingClass.getMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
    }

    private fun getDefaultViewBinding(
        vhClass: Class<BaseViewHolder<T, *>>,
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        attachToParent: Boolean
    ): ViewBinding {
        val inflater = inflaterViewBinding ?: getMethodInflateViewBinding(vhClass).also {
            inflaterViewBinding = it
        }
        return inflater.invoke(null, layoutInflater, parent, attachToParent) as ViewBinding
    }

    private fun getDefaultViewHolder(): Class<BaseViewHolder<T, *>> {
        return defaultVHClass
            ?: ((javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<BaseViewHolder<T, *>>).also {
                defaultVHClass = it
            }
    }

    fun getRecyclerViewOrientation(recyclerView: RecyclerView): Int {
        return recyclerView.layoutManager.let {
            when (it) {
                is LinearLayoutManager -> {
                    it.orientation
                }

                is StaggeredGridLayoutManager -> {
                    it.orientation
                }

                else -> {
                    RecyclerView.VERTICAL
                }
            }
        }
    }

    fun setLoadMoreListener(loadMoreRecyclerViewListener: LoadMoreRecyclerViewListener) {
        this.loadMoreRecyclerViewListener = loadMoreRecyclerViewListener
    }

    open fun getViewType(position: Int): Int = 0

    open fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<T, *> {
        val vhClass = getDefaultViewHolder()
        val defaultItemBinding = getDefaultViewBinding(vhClass, layoutInflater, parent, false)
        return vhClass.constructors[0].newInstance(defaultItemBinding) as BaseViewHolder<T, *>
    }
}