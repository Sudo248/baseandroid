package com.sudo248.base_android.base

import androidx.annotation.Nullable
import androidx.annotation.StringRes

interface BaseViewController {
    fun getString(@StringRes resId: Int): String

    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String

}