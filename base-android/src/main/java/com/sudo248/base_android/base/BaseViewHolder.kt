package com.sudo248.base_android.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

abstract class BaseViewHolder<in T : Any, VB : ViewBinding>(
    private val _binding: VB? = null,
    view: View? = null
) :
    RecyclerView.ViewHolder(
        _binding?.root ?: view ?: throw Exception("Item view or item binding is required")
    ) {
    protected val binding: VB get() = _binding!!

    abstract fun onBind(item: T)
}