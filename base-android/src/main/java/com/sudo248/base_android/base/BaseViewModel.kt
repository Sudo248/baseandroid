package com.sudo248.base_android.base

import androidx.lifecycle.ViewModel
import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.bindUiState
import com.sudo248.base_android.navigation.Navigator
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.lang.NullPointerException
import java.lang.ref.WeakReference
import kotlin.coroutines.CoroutineContext

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

abstract class BaseViewModel<Directions> : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main.immediate + SupervisorJob()

    override fun onCleared() {
        super.onCleared()
        _navigator.clear()
        _viewController.clear()
        cancel("onCleared ${this::class.simpleName} -> cancel coroutine ${coroutineContext[CoroutineName]}")
    }

    private lateinit var _navigator: WeakReference<Navigator<Directions>>

    protected val navigator: Navigator<Directions>
        get() = _navigator.get() ?: throw NullPointerException("Navigator be cleared")

    private lateinit var _viewController: WeakReference<BaseViewController>

    protected val viewController: BaseViewController
        get() = _viewController.get() ?: throw NullPointerException("ViewController be cleared")

    private val _uiState = MutableStateFlow(SingleEvent(UiState.IDLE))
    val uiState: StateFlow<SingleEvent<UiState>> = _uiState

    var error: SingleEvent<Exception?> = SingleEvent(null)
        protected set

    fun setupNavigator(navigator: Navigator<Directions>) {
        this._navigator = WeakReference(navigator)
    }

    fun setState(state: UiState) {
        _uiState.value = SingleEvent(state)
    }

    fun setViewController(viewController: BaseViewController) {
        this._viewController = WeakReference(viewController)
    }

    suspend fun emitState(state: UiState) {
        _uiState.emit(SingleEvent(state))
    }

    fun uiState(state: MutableStateFlow<SingleEvent<UiState>> = _uiState, handler: suspend () -> DataState<*, *>) = launch {
        state.emit(SingleEvent(UiState.LOADING))
        val result = try {
            handler.invoke()
        } catch (e: Exception) {
            DataState.Error(e)
        }
        if (result.isError) error = SingleEvent(result.error())
        result.bindUiState(state)
    }

    fun navigator() = navigator

    fun <T : BaseViewController> viewController(): T = _viewController.get() as T

}