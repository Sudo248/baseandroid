package com.sudo248.base_android.base

import androidx.navigation.NavDirections
import com.sudo248.base_android.navigation.IntentDirections

sealed class EmptyViewModel<Directions> : BaseViewModel<Directions>() {
    class Activity : EmptyViewModel<IntentDirections>()

    class Fragment : EmptyViewModel<NavDirections>()
}