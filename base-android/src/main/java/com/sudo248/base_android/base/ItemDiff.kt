package com.sudo248.base_android.base


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 00:01 - 17/02/2023
 */
interface ItemDiff {
    fun isItemTheSame(other: ItemDiff): Boolean
    fun isContentTheSame(other: ItemDiff): Boolean
}