package com.sudo248.base_android.base

interface LoadMoreRecyclerViewListener {
    fun onLoadMore(page: Int, itemCount: Int)
}