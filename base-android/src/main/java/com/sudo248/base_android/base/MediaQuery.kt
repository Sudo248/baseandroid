package com.sudo248.base_android.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.*
import androidx.core.view.isGone
import com.sudo248.base_android.ktx.visible

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

class MediaQuery private constructor(private val activity: Activity) {
    companion object {
        fun of(activity: Activity): MediaQuery {
            return MediaQuery(activity)
        }

        private const val TAG_STATUS_BAR = "TAG_STATUS_BAR"
        private const val TAG_OFFSET = "TAG_OFFSET"
        private const val KEY_OFFSET = -123

        const val FLAG_SHOW_STATUS_BAR = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        const val FLAG_HIDE_STATUS_BAR = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_FULLSCREEN

        const val FLAG_SHOW_NAVIGATION_BAR = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        const val FLAG_HIDE_NAVIGATION_BAR = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN

        const val FLAG_FULL_SCREEN = 1536

        const val FLAG_DEFAULT_SCREEN = 4615
    }

    private val window: Window = activity.window

    private val decorView: View = window.decorView

    private val activityResources = activity.resources

    private val applicationResource = activity.application.resources

    /**
     * size of screen
     */
    val size: DisplayMetrics
        get() = activityResources.displayMetrics

    /**
     * height of screen
     */
    val height: Int
        get() = size.heightPixels

    /**
     * width of screen
     */
    val width: Int
        get() = size.widthPixels

    val statusBarHeight: Int
        get() {
            val resources = activity.resources
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            return resources.getDimensionPixelSize(resourceId)
        }

    var isStatusBarVisible: Boolean
        get() = getStatusBarVisible()
        set(value) {
            if (value) {
                showStatusBar()
            } else {
                hideStatusBarView()
            }
        }

    var isStatusBarLightMode: Boolean
        get() = (decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) != 0
        set(value) {
                val visible = decorView.systemUiVisibility
                decorView.systemUiVisibility = if (value) {
                    visible or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    visible and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                }
        }

    var statusBarColor: Int
        get() = window.statusBarColor
        set(value) {
            window.statusBarColor = value
        }

    /**
     * Use [statusBarColor] instead of
     * */
    @Deprecated("Not should use")
    fun setStatusBarColor(color: Int, isDecor: Boolean = false) {
        applyStatusBarColor(color, isDecor)
    }

    var isFullScreen: Boolean
        get() = getFullScreen()
        set(value) {
            if (value) {
                fullScreen()
            } else {
                defaultScreen()
            }
        }

    @SuppressLint("NewApi")
    private fun getFullScreen(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            decorView.rootWindowInsets?.let {
                it.isVisible(WindowInsets.Type.statusBars()) &&
                        it.isVisible(WindowInsets.Type.navigationBars())
            } ?: true
        } else {
            decorView.systemUiVisibility == FLAG_FULL_SCREEN
        }
    }

    @SuppressLint("NewApi")
    private fun fullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.apply {
                show(WindowInsets.Type.statusBars())
                show(WindowInsets.Type.navigationBars())
            }
        } else decorView.systemUiVisibility = FLAG_FULL_SCREEN
    }

    @SuppressLint("NewApi")
    private fun defaultScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.apply {
                hide(WindowInsets.Type.statusBars())
                hide(WindowInsets.Type.navigationBars())
            }
        } else decorView.systemUiVisibility = FLAG_DEFAULT_SCREEN
    }

    @SuppressLint("NewApi")
    private fun getStatusBarVisible(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            decorView.rootWindowInsets
                ?.isVisible(WindowInsets.Type.statusBars()) ?: true
        } else {
            decorView.systemUiVisibility == FLAG_SHOW_STATUS_BAR
        }
    }

    @SuppressLint("NewApi")
    private fun showStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.show(WindowInsets.Type.statusBars())
        } else {
            decorView.systemUiVisibility = FLAG_SHOW_STATUS_BAR
        }
    }

    @SuppressLint("NewApi")
    private fun hideStatusBarView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            decorView.systemUiVisibility = FLAG_HIDE_STATUS_BAR
        }
    }

    private fun addMarginTopEqualStatusBarHeight() {
        val withTag = decorView.findViewWithTag<View>(TAG_OFFSET)
        withTag.tag = TAG_OFFSET
        val haveSetOffset = withTag.getTag(KEY_OFFSET)
        if (haveSetOffset is Boolean) {
            val layoutParam = withTag.layoutParams as ViewGroup.MarginLayoutParams
            layoutParam.setMargins(
                layoutParam.leftMargin,
                layoutParam.topMargin + statusBarHeight,
                layoutParam.rightMargin,
                layoutParam.bottomMargin
            )
            withTag.setTag(KEY_OFFSET, true)
        }
    }

    private fun subtractMarginTopEqualStatusBarHeight() {
        val withTag = decorView.findViewWithTag<View>(TAG_OFFSET)
        withTag.tag = TAG_OFFSET
        val haveSetOffset = withTag.getTag(KEY_OFFSET)
        if (haveSetOffset is Boolean) {
            val layoutParam = withTag.layoutParams as ViewGroup.MarginLayoutParams
            layoutParam.setMargins(
                layoutParam.leftMargin,
                layoutParam.topMargin - statusBarHeight,
                layoutParam.rightMargin,
                layoutParam.bottomMargin
            )
            withTag.setTag(KEY_OFFSET, false)
        }
    }

    private fun applyStatusBarColor(color: Int, isDecor: Boolean) {
        val parent =
            (if (isDecor) decorView else window.findViewById(android.R.id.content)) as ViewGroup
        val statusBarView = parent.findViewWithTag<View?>(TAG_STATUS_BAR)
        if (statusBarView != null) {
            if (statusBarView.isGone) {
                statusBarView.visible()
            }
            statusBarView.setBackgroundColor(color)
        } else {
            parent.addView(createStatusBarView(color))
        }
    }

    private fun createStatusBarView(color: Int): View {
        val statusBarView = View(activity)
        statusBarView.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            statusBarHeight
        )
        statusBarView.setBackgroundColor(color)
        statusBarView.tag = TAG_STATUS_BAR
        return statusBarView
    }

    /**
     * @return action bar height
     */

    val actionBarHeight: Int
        get() {
            val typedValue = TypedValue()
            return if (activity.applicationContext.theme.resolveAttribute(
                    android.R.attr.actionBarSize,
                    typedValue,
                    true
                )
            ) {
                TypedValue.complexToDimensionPixelSize(typedValue.data, size)
            } else 0
        }

    /**
     * @return navigation bar height
     */

    val navigationBarHeight: Int
        get() {
            val resources = activity.applicationContext.resources
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            return if (resourceId != 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        }

    var isNavigationBarVisible: Boolean
        get() = getNavigationBarVisible()
        set(value) {
            if (value) {
                showNavigationBarView()
            } else {
                hideNavigationBarView()
            }
        }

    @SuppressLint("NewApi")
    private fun getNavigationBarVisible(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            decorView.rootWindowInsets
                ?.isVisible(WindowInsets.Type.navigationBars()) ?: true
        } else {
            decorView.systemUiVisibility == FLAG_SHOW_NAVIGATION_BAR
        }
    }

    @SuppressLint("NewApi")
    private fun hideNavigationBarView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(
                WindowInsets.Type.navigationBars()
            )
        } else {
            decorView.systemUiVisibility = FLAG_HIDE_NAVIGATION_BAR
        }
    }

    @SuppressLint("NewApi")
    private fun showNavigationBarView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.show(
                WindowInsets.Type.navigationBars()
            )
        } else {
            decorView.systemUiVisibility = FLAG_SHOW_NAVIGATION_BAR
        }
    }

    val configuration: Configuration
        get() = activity.resources.configuration

    val orientation: Int
        get() = configuration.orientation

    val isOrientationPortrait: Boolean
        get() = orientation == Configuration.ORIENTATION_PORTRAIT

    val fontScale: Float
        get() = configuration.fontScale

    val theme: Resources.Theme
        get() = activity.applicationContext.theme

    fun setTheme(resId: Int) {
        activity.applicationContext.setTheme(resId)
    }
}