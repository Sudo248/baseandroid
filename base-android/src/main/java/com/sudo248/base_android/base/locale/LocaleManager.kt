package com.sudo248.base_android.base.locale

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import com.sudo248.base_android.ktx.string
import com.sudo248.base_android.utils.AppSharedPreference
import java.util.Locale

class LocaleManager private constructor(private val context: Context) {

    companion object {

        private var appLanguageCode by AppSharedPreference.get().string()

        var enableSupportLanguage: Boolean = false
            private set

        fun of(context: Context): LocaleManager = LocaleManager(context)

        fun of(activity: Activity): LocaleManager = LocaleManager(activity)

        fun setEnableSupportLanguage(enableSupportLanguage: Boolean) {
            this.enableSupportLanguage = enableSupportLanguage
        }

        fun getCurrentLanguageCode(): String = appLanguageCode
    }

    private fun updateResourceConfiguration(locale: Locale): Context {
        val config = Configuration(context.resources.configuration)
        config.setLayoutDirection(locale)
        config.setLocale(locale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val localeList = LocaleList(locale)
            LocaleList.setDefault(localeList)
            config.setLocales(localeList)
        }
        return context.createConfigurationContext(config)
    }

    private fun updateResourceConfigurationLegacy(locale: Locale): Context {
        return context.apply {
            val config = Configuration(context.resources.configuration)
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
        }
    }

    private fun updateBaseContextLocale(locale: Locale): Context {
        Locale.setDefault(locale)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResourceConfiguration(locale)
        } else {
            updateResourceConfigurationLegacy(locale)
        }
    }

    fun getLocalizedContext(): Context {
        val locale = Locale(appLanguageCode)
        return updateBaseContextLocale(locale)
    }

    fun applyLocalized(languageCode: String, bundle: Bundle? = null) {
        if (languageCode != appLanguageCode && context is Activity) {
            appLanguageCode = languageCode
            context.finish()
            val intent = Intent(context, context.javaClass).apply {
                bundle?.let {
                    putExtras(bundle)
                }
            }
            context.startActivity(intent)
        }
    }
}
