package com.sudo248.base_android.connectivity

import kotlinx.coroutines.flow.Flow


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 10:46 - 25/02/2023
 */
interface ConnectivityObserver {

    fun observer(): Flow<Status>

    enum class Status {
        AVAILABLE, UNAVAILABLE, LOSING, LOST;

        fun hasNetwork(): Boolean = this == AVAILABLE
    }

}