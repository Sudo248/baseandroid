package com.sudo248.base_android.core

/**
 * Build env of application
 * ex: config in build gradle
 *
 */
enum class BuildEnv {
    DEBUG,
    STAGING,
    PRODUCTION
}