package com.sudo248.base_android.core

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

/**
 * Description for state of screen
 * - [IDLE]: this screen in background
 * - [SUCCESS]: this screen in foreground and run
 * - [LOADING]: this screen in loading
 * - [ERROR]: this screen in error (maybe show a dialog)
 */

enum class UiState {
    /**
     * call [update] to change state to [SUCCESS]
     */
    IDLE {
        override fun update(newState: UiState?): UiState = newState ?: SUCCESS
    },

    /**
     * call [update] to change state to [LOADING]
     */
    SUCCESS {
        override fun update(newState: UiState?): UiState = newState ?: LOADING
    },
    /**
     * call [update] to change state to [SUCCESS]
     */
    LOADING {
        override fun update(newState: UiState?): UiState = newState ?: SUCCESS
    },
    /**
     * call [update] to change state to [SUCCESS]
     */
    ERROR {
        override fun update(newState: UiState?): UiState = newState ?: SUCCESS
    };

    abstract fun update(newState: UiState? = null): UiState

    fun onState(
        idle: () -> Unit = {},
        loading: () -> Unit = {},
        success: () -> Unit = {},
        error: () -> Unit = {}
    ) {
        when (this) {
            SUCCESS -> success()
            LOADING -> loading()
            ERROR -> error()
            else -> idle()
        }
    }
}