package com.sudo248.base_android.core.bus

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.core.bus.converter.DataConverter
import java.lang.reflect.Type
import kotlin.reflect.KClass

internal class Executor<E : Any>(
    val eventType: Type,
    val subscriber: SudoSubscriber<E, *>,
    val converter: DataConverter
) {

    private fun <OUT : Any> convert(input: Any, outputType: Type): OUT {
        val eventJson = converter.toJson(input)
        return converter.fromJson(eventJson, outputType)
    }

    suspend fun <R : Any> execute(
        event: Any,
        resultKClass: KClass<R>
    ): DataState<R, Exception> {
        return try {
            val expectedEvent = convert<E>(event, eventType)
            val result = subscriber.onReceiveEvent(expectedEvent)
            if (result.isSuccess) {
                val expectedResult = convert<R>(result.requireData(), resultKClass.java)
                DataState.Success(expectedResult)
            } else {
                DataState.Error(result.error())
            }
        } catch (e: Exception) {
            DataState.Error(e)
        }
    }
}