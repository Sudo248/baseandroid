package com.sudo248.base_android.core.bus

import com.sudo248.base_android.core.DataState


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 17:15 - 25/02/2023
 */
abstract class HandleSubscriber<in EVENT : Any, out RESULT : Any> : SudoSubscriber<EVENT, RESULT>() {

    abstract suspend fun handle(event: EVENT): DataState<RESULT, Exception>

    override suspend fun onReceiveEvent(event: EVENT): DataState<RESULT, Exception> = handle(event)
}