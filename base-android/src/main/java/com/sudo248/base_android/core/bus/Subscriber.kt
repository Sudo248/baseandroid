package com.sudo248.base_android.core.bus

import com.sudo248.base_android.core.DataState


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 17:57 - 25/02/2023
 */
abstract class Subscriber<in EVENT : Any> : SudoSubscriber<EVENT, Unit>() {
    abstract suspend fun onReceive(event: EVENT)

    override suspend fun onReceiveEvent(event: EVENT): DataState<Unit, Exception> {
        onReceive(event)
        return DataState.Success(Unit)
    }
}