package com.sudo248.base_android.core.bus

import com.sudo248.base_android.app.BaseApplication
import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.core.bus.converter.DataConverter
import com.sudo248.base_android.core.bus.converter.GsonDataConverter
import com.sudo248.base_android.core.exception.SudoBusException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import kotlin.reflect.KClass


/**
 * **Created by**
 *
 * ========================================
 * event =>                                 SudoBus
 * ========================================
 *    |executor|      |executor|            Executor (map data event and result)
 * | subscriber |   | subscriber |          Implement subscriber
 *
 *
 * @author *Sudo248*
 * @since 18:01 - 25/02/2023
 */
class SudoBus private constructor(
    private val subscriberManager: SudoSubscriberManager,
    private val converter: DataConverter
) : CoroutineScope by MainScope() {

    inline fun <reified E : Any> subscribe(
        topic: String,
        subscriber: Subscriber<E>
    ): String {
        return subscribe(
            topic = topic,
            eventType = E::class.java,
            subscriber = subscriber
        )
    }

    inline fun <reified E : Any> subscribeHandler(
        topic: String,
        subscriber: HandleSubscriber<E, *>,
    ): String {
        return subscribe(
            topic = topic,
            eventType = E::class.java,
            subscriber = subscriber
        )
    }

    fun subscribe(
        topic: String,
        eventType: Type,
        subscriber: SudoSubscriber<*, *>
    ): String {
        return subscriberManager.addSubscriber(
            topic = topic,
            eventType = eventType,
            subscriber = subscriber,
            dataConverter = converter
        )
    }

    fun unSubscriber(topic: String, subscriber: SudoSubscriber<*, *>) {
        unSubscriber(topic, subscriber.subscriberId)
    }

    fun unSubscriber(topic: String, subscriberId: String) {
        subscriberManager.removeSubscriber(topic, subscriberId)
    }

    fun unSubscribers(topic: String, subscriberIds: List<String>) {
        subscriberIds.forEach { subscriberId ->
            unSubscriber(topic, subscriberId)
        }
    }

    fun unSubscribeAll(topic: String) {
        subscriberManager.removeAll(topic)
    }


    fun publish(topic: String, event: Any) {
        ensureActive()
        launch {
            subscriberManager.getSubscriberExecutors(topic)?.forEach {
                launch {
                    it.execute(
                        event = event,
                        Unit::class
                    )
                }
            }
        }
    }

    inline fun <reified R : Any> request(
        topic: String,
        event: Any,
        noinline onResult: (DataState<R, Exception>) -> Unit,
    ) {
        return request(
            topic = topic,
            event = event,
            resultKClass = R::class,
            onResult = onResult
        )
    }

    suspend inline fun <reified R : Any> request(
        topic: String,
        event: Any,
    ): DataState<R, Exception> {
        return requestAsync<R>(topic, event).await()
    }

    suspend inline fun <reified R : Any> requestAsync(
        topic: String,
        event: Any,
    ) = requestAsync(
        topic = topic,
        event = event,
        resultKClass = R::class
    )

    fun <R : Any> request(
        topic: String,
        event: Any,
        resultKClass: KClass<R>,
        onResult: (DataState<R, Exception>) -> Unit,
    ) {
        launch {
            val result = requestAsync(topic, event, resultKClass).await()
            ensureActive()
            onResult(result)
        }
    }

    suspend fun <R : Any> requestAsync(
        topic: String,
        event: Any,
        resultKClass: KClass<R>,
    ): Deferred<DataState<R, Exception>> = coroutineScope {
        async {
            val executor = subscriberManager.getHandleSubscriberExecutor(topic)
            executor?.execute(event, resultKClass)
                ?: DataState.Error(SudoBusException.NotFoundSubscriberException(topic))
        }
    }

    companion object {

        private var instance: SudoBus? = null

        fun createInstance(dataConverter: DataConverter = GsonDataConverter()): SudoBus {
            return SudoBus(
                subscriberManager = SudoSubscriberManager(),
                converter = dataConverter
            ).also {
                instance = it
            }
        }

        fun getInstance(): SudoBus {
            return instance ?: createInstance()
        }
    }
}