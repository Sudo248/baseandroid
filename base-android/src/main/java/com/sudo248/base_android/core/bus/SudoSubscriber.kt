package com.sudo248.base_android.core.bus

import com.sudo248.base_android.core.DataState
import java.util.UUID


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 16:29 - 25/02/2023
 */
abstract class SudoSubscriber<in EVENT : Any, out RESULT : Any> {
    internal val subscriberId: String = UUID.randomUUID().toString()

    internal abstract suspend fun onReceiveEvent(event: EVENT): DataState<RESULT, Exception>
}