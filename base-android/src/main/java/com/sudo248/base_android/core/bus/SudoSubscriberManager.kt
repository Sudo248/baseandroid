package com.sudo248.base_android.core.bus

import android.util.Log
import com.sudo248.base_android.core.bus.converter.DataConverter
import java.lang.reflect.Type


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 17:21 - 25/02/2023
 */
internal class SudoSubscriberManager {
    private val executors: MutableMap<String, MutableList<Executor<*>>> = hashMapOf()

    fun <E : Any> addSubscriber(
        topic: String,
        eventType: Type,
        subscriber: SudoSubscriber<E, *>,
        dataConverter: DataConverter,
    ): String {
        val executor = Executor<E>(
            eventType = eventType,
            subscriber = subscriber,
            converter = dataConverter
        )
        if (executors.containsKey(topic)) {
            executors[topic]?.add(executor)
            if (subscriber is HandleSubscriber) {
                Log.w(
                    "SudoSubscriberManager",
                    "Duplicate SubscribeHandler. Only first handler can handle request"
                )
            }
        } else {
            executors[topic] = mutableListOf(executor)
        }
        return subscriber.subscriberId
    }

    fun removeSubscriber(topic: String, subscriber: SudoSubscriber<*, *>) {
        executors[topic]?.removeIf { it.subscriber.subscriberId == subscriber.subscriberId }
    }

    fun removeSubscriber(topic: String, subscriberId: String) {
        executors[topic]?.removeIf { it.subscriber.subscriberId == subscriberId }
    }

    fun removeAll(topic: String) {
        executors[topic]?.clear()
    }

    fun getSubscriberExecutors(topic: String): List<Executor<*>>? {
        return executors[topic]?.filter { it.subscriber is Subscriber<*> }
    }

    fun getHandleSubscriberExecutor(
        topic: String
    ): Executor<*>? {
        return executors[topic]?.firstOrNull { it.subscriber is HandleSubscriber<*, *> }
    }
}