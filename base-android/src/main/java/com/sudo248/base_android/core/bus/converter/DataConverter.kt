package com.sudo248.base_android.core.bus.converter

import java.lang.reflect.Type

interface DataConverter {
    fun <T : Any> fromJson(json: String, type: Type): T

    fun toJson(source: Any): String
}