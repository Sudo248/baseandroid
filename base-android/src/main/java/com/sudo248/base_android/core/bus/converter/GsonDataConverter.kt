package com.sudo248.base_android.core.bus.converter

import com.google.gson.Gson
import java.lang.reflect.Type

class GsonDataConverter : DataConverter {
    private val gson = Gson()
    override fun <T : Any> fromJson(json: String, type: Type): T {
        return gson.fromJson(json, type)
    }

    override fun toJson(source: Any): String {
        return gson.toJson(source)
    }

}