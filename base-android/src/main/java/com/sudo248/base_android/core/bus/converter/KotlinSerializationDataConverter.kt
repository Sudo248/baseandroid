package com.sudo248.base_android.core.bus.converter

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import java.lang.reflect.Type

@Suppress("UNCHECKED_CAST")
class KotlinSerializationDataConverter : DataConverter {
    private val json = Json { ignoreUnknownKeys = true }

    @OptIn(ExperimentalSerializationApi::class)
    override fun <T : Any> fromJson(json: String, type: Type): T {
        val loader = Json.serializersModule.serializer(type)
        return this.json.decodeFromString(loader, json) as T
    }

    @OptIn(ExperimentalSerializationApi::class)
    override fun toJson(source: Any): String {
        val loader = Json.serializersModule.serializer(source.javaClass)
        return json.encodeToString(loader, source)
    }
}