package com.sudo248.base_android.core.event

import androidx.lifecycle.Observer


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 00:41 - 18/02/2023
 */
class SingleEventObserver <T> (private val onChanged: (T) -> Unit) : Observer<SingleEvent<T>> {
    override fun onChanged(event: SingleEvent<T>?) {
        event?.value?.let {
            onChanged.invoke(it)
        }
    }
}