package com.sudo248.base_android.core.exception


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 02:04 - 26/02/2023
 */
sealed class SudoBusException(message: String) : Exception(message) {

    class UnknownException(eventName: String, message: String?) :
        SudoBusException("Unknown exception $message when uiState event $eventName")

    class NotFoundSubscriberException(eventName: String) :
        SudoBusException("Not found subscriber for event $eventName")

    class DuplicateSubscribeHandler(eventName: String) :
        SudoBusException("Duplicate subscribe handler for event $eventName (Each event should has only one handler)")
}