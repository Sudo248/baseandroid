package com.sudo248.base_android.data.api


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 01:17 - 25/02/2023
 */

inline fun <reified S> api(block: ApiServiceBuilder<S>.() -> Unit): S =
    ApiServiceBuilder(S::class.java).getConfigFromAnnotation().apply(block).build()

inline fun <reified S> ApiService(): S =
    ApiServiceBuilder(S::class.java).getConfigFromAnnotation().build()
