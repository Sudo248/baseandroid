package com.sudo248.base_android.data.exception


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 07:34 - 06/03/2023
 */
data class ApiException(
    val statusCode: Int,
    override val message: String,
    val data: Any? = null
) : Exception(message) {
    companion object
}