package com.sudo248.base_android.data.gson

import android.os.Build
import androidx.annotation.RequiresApi
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class LocalTimeConverter : JsonSerializer<LocalTime>, JsonDeserializer<LocalTime> {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun serialize(
        src: LocalTime?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return JsonPrimitive(DateTimeFormatter.ISO_LOCAL_TIME.format(src))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): LocalTime {
        return DateTimeFormatter.ISO_LOCAL_TIME.parse(json?.asString, LocalTime::from)
    }
}