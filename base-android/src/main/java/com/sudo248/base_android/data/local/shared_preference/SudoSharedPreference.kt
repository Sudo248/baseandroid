package com.sudo248.base_android.data.local.shared_preference

import android.content.SharedPreferences

interface SudoSharedPreference : SharedPreferences {

    fun put(key: String, value: Any): SudoSharedPreference

    fun putString(key: String, value: String?): SudoSharedPreference

    fun putInt(key: String, value: Int): SudoSharedPreference

    fun putLong(key: String, value: Long): SudoSharedPreference

    fun putFloat(key: String, value: Float): SudoSharedPreference

    fun putBoolean(key: String, value: Boolean): SudoSharedPreference

    fun putStringSet(key: String, value: Set<String>): SudoSharedPreference

    fun remove(key: String): SudoSharedPreference

    fun clear(): SudoSharedPreference

    fun putObject(key: String, value: Any): SudoSharedPreference

    fun <T> getObject(key: String, classValue: Class<T>, defValue: T? = null): T?

}