package com.sudo248.base_android.data.local.shared_preference

import android.content.SharedPreferences
import com.google.gson.Gson

internal class SudoSharedPreferenceImpl(
    private val pref: SharedPreferences
) : SudoSharedPreference, SharedPreferences by pref {

    private val gson: Gson by lazy {
        Gson()
    }

    private fun edit(
        commit: Boolean = false,
        transform: SharedPreferences.Editor.() -> Unit
    ): SudoSharedPreference {
        val editor = edit()
        transform(editor)
        if (commit) editor.commit() else editor.apply()
        return this
    }

    override fun put(key: String, value: Any): SudoSharedPreference {
        when (value) {
            is Boolean -> putBoolean(key, value)
            is String -> putString(key, value)
            is Int -> putInt(key, value)
            is Long -> putLong(key, value)
            is Float -> putFloat(key, value)
            else -> {
                val valueString = gson.toJson(value)
                putString(key, valueString)
            }
        }
        return this
    }

    override fun putString(key: String, value: String?): SudoSharedPreference = edit { putString(key, value) }

    override fun putInt(key: String, value: Int): SudoSharedPreference = edit { putInt(key, value) }

    override fun putLong(key: String, value: Long): SudoSharedPreference = edit { putLong(key, value) }

    override fun putFloat(key: String, value: Float): SudoSharedPreference = edit { putFloat(key, value) }

    override fun putBoolean(key: String, value: Boolean): SudoSharedPreference = edit { putBoolean(key, value) }

    override fun putStringSet(key: String, value: Set<String>): SudoSharedPreference = edit { putStringSet(key, value) }

    override fun remove(key: String): SudoSharedPreference = edit { remove(key) }

    override fun clear(): SudoSharedPreference = edit { clear() }

    override fun putObject(key: String, value: Any): SudoSharedPreference {
        val objectString = gson.toJson(value)
        return putString(key, objectString)
    }

    override fun <T> getObject(key: String, classValue: Class<T>, defValue: T?): T? {
        val valueString = getString(key, null)
        return gson.fromJson(valueString, classValue)
    }
}