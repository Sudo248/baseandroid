package com.sudo248.base_android.data.local.shared_preference.factory

import android.content.Context
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreference
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreferenceImpl

object SudoSharedPreferenceFactory {
    fun create(
        context: Context,
        namePref: String,
        modeOpen: Int = Context.MODE_PRIVATE
    ): SudoSharedPreference {
        val pref = context.getSharedPreferences(namePref, modeOpen)
        return SudoSharedPreferenceImpl(pref)
    }
}