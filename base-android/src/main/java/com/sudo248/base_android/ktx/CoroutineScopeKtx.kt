package com.sudo248.base_android.ktx

import android.util.Log
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext


/**
 *  ## Created by
 * @author *Sudo248*
 * @since 23:21 - 21/08/2022
 */

fun <T> CoroutineScope.asyncHandler (
    context: CoroutineContext = EmptyCoroutineContext,
    handleException: ((coroutineContext: CoroutineContext, throwable: Throwable) -> Unit)? =  null,
    block: suspend CoroutineScope.() -> T
): Deferred<T> {
    lateinit var result: Deferred<T>
    val coroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        if (result.isActive) result.cancel()
        Log.e(coroutineContext[CoroutineName.Key]?.name, "asyncHandler: ", throwable)
        handleException?.invoke(coroutineContext, throwable)
    }
    result = async(context + coroutineExceptionHandler, block = block)
    return result
}

/**
 * When throw an exception this coroutine must be cancel
 *
 */

fun CoroutineScope.launchHandler (
    context: CoroutineContext = EmptyCoroutineContext,
    handleException: ((coroutineContext: CoroutineContext, throwable: Throwable) -> Unit)? =  null,
    block: suspend CoroutineScope.() -> Unit
): Job {
    lateinit var result: Job
    val coroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
        Log.e(coroutineContext[CoroutineName.Key]?.name, "launchHandler: ", throwable)
        if (result.isActive) result.cancel()
        handleException?.invoke(coroutineContext, throwable)
    }
    result = launch(context + coroutineExceptionHandler, block = block)
    return result
}


