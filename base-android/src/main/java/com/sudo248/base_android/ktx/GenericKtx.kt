package com.sudo248.base_android.ktx

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.sudo248.base_android.navigation.ActionOnlyIntentDirections
import com.sudo248.base_android.navigation.IntentDirections
import kotlin.reflect.KClass

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

val <T : Any> T.TAG: String
    get() = this::class.java.canonicalName ?: this::class.java.simpleName

/**
 * Create action intent direction to [T]
 */
fun <T : AppCompatActivity> KClass<T>.createActionIntentDirections(): IntentDirections {
    return ActionOnlyIntentDirections(this)
}

/**
 * Create action intent direction to [T] with [intentBuilder]
 */
fun <T : AppCompatActivity> KClass<T>.createActionIntentDirections(
    intentBuilder: Intent.() -> Unit
): IntentDirections {
    val action = ActionOnlyIntentDirections(this)
    action.intent.apply { intentBuilder() }
    return action
}