package com.sudo248.base_android.ktx

import com.google.gson.annotations.SerializedName
import java.lang.reflect.Field

fun Field.serializedName(): String = getAnnotation(SerializedName::class.java)?.value ?: name