package com.sudo248.base_android.ktx

import android.util.Base64
import java.nio.charset.Charset


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 20:50 - 02/03/2023
 */

fun String.decode(): String {
    return Base64.decode(this, Base64.DEFAULT).toString(Charsets.UTF_8)
}

fun String.encode(): String {
    return Base64.encodeToString(this.toByteArray(Charsets.UTF_8), Base64.DEFAULT)
}