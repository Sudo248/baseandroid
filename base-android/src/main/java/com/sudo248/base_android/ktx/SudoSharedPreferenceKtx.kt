package com.sudo248.base_android.ktx

import android.content.SharedPreferences
import com.google.gson.Gson
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

inline fun <reified T : Any> SudoSharedPreference.get(key: String, defValue: T? = null): T? {
    return when (T::class) {
        String::class -> getString(key, defValue as? String) as T?
        Int::class -> getInt(key, defValue as? Int ?: -1) as T
        Float::class -> getFloat(key, defValue as? Float ?: -1f) as T
        Long::class -> getLong(key, defValue as? Long ?: -1L) as T
        Boolean::class -> getBoolean(key, defValue as? Boolean ?: false) as T
        else -> {
            val value = getString(key, null)
            if (value.isNullOrEmpty()) {
                defValue
            } else {
                Gson().fromJson(value, T::class.java)
            }
        }
    }
}

inline fun <reified T : Any> SudoSharedPreference.registerValueChangeListener(
    key: String,
    crossinline onChange: (T) -> Unit
): SharedPreferences.OnSharedPreferenceChangeListener {
    if (!contains(key)) throw Exception("SharedPreference don't contain key $key")
    val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, prefKey ->
        if (prefKey == key) {
            val value: T = when (T::class) {
                String::class -> getString(key, "") as T
                Long::class -> getLong(key, -1) as T
                Int::class -> getInt(key, -1) as T
                Boolean::class -> getBoolean(key, false) as T
                Float::class -> getFloat(key, -1f) as T
                else -> {
                    Gson().fromJson(key, T::class.java)
                }
            }
            onChange(value)
        }
    }
    registerOnSharedPreferenceChangeListener(listener)
    return listener
}

fun SudoSharedPreference.unregisterValueChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
    unregisterOnSharedPreferenceChangeListener(listener)
}

private inline fun <T> SudoSharedPreference.delegate(
    defaultValue: T,
    key: String?,
    crossinline getter: SudoSharedPreference.(String, T) -> T,
    crossinline setter: SudoSharedPreference.(String, T) -> SudoSharedPreference
): ReadWriteProperty<Any, T> {
    return object : ReadWriteProperty<Any, T> {
        override fun getValue(thisRef: Any, property: KProperty<*>) =
            getter(key ?: property.name, defaultValue)

        override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
            setter(key ?: property.name, value)
        }
    }
}

/**
 * Need to separate [delegate] and [delegateWithNull] to optimize check contain key or null value
 *
 * every get or set value to [SudoSharedPreference]
 */
private inline fun <T> SudoSharedPreference.delegateWithNull(
    key: String?,
    crossinline getter: (String) -> T,
    crossinline setter: (String, T) -> Unit,
): ReadWriteProperty<Any, T?> {
    return object : ReadWriteProperty<Any, T?> {
        override fun getValue(thisRef: Any, property: KProperty<*>): T? {
            val prefKey = key ?: property.name
            return if (contains(prefKey)) {
                getter(prefKey)
            } else null
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: T?) {
            val prefKey = key ?: property.name
            if (value == null) {
                remove(prefKey)
            } else {
                setter(prefKey, value)
            }
        }
    }
}

fun SudoSharedPreference.int(defaultValue: Int = Int.MIN_VALUE, key: String? = null) =
    delegate(defaultValue, key, SudoSharedPreference::getInt, SudoSharedPreference::putInt)

fun SudoSharedPreference.float(defaultValue: Float = Float.MIN_VALUE, key: String? = null) =
    delegate(defaultValue, key, SudoSharedPreference::getFloat, SudoSharedPreference::putFloat)

fun SudoSharedPreference.long(defaultValue: Long = Long.MIN_VALUE, key: String? = null) =
    delegate(defaultValue, key, SudoSharedPreference::getLong, SudoSharedPreference::putLong)

fun SudoSharedPreference.boolean(defaultValue: Boolean = false, key: String? = null) =
    delegate(defaultValue, key, SudoSharedPreference::getBoolean, SudoSharedPreference::putBoolean)

fun SudoSharedPreference.string(defaultValue: String = "", key: String? = null) =
    delegate(
        defaultValue,
        key,
        getter = { prefKey, prefDefaultValue -> getString(prefKey, prefDefaultValue)!! },
        setter = SudoSharedPreference::putString
    )

fun SudoSharedPreference.intOrNull(key: String? = null) =
    delegateWithNull(
        key,
        setter = { prefKey, value ->
            putInt(prefKey, value)
        },
        getter = {
            getInt(it, -1)
        },
    )

fun SudoSharedPreference.floatOrNull(key: String? = null) =
    delegateWithNull(
        key,
        setter = { prefKey, value ->
            putFloat(prefKey, value)
        },
        getter = {
            getFloat(it, -1f)
        },
    )

fun SudoSharedPreference.longOrNull(key: String? = null) =
    delegateWithNull(
        key,
        setter = { prefKey, value ->
            putLong(prefKey, value)
        },
        getter = {
            getLong(it, -1L)
        },
    )

fun SudoSharedPreference.booleanOrNull(key: String? = null) =
    delegateWithNull(
        key,
        setter = { prefKey, value ->
            putBoolean(prefKey, value)
        },
        getter = {
            getBoolean(it, false)
        },
    )

fun SudoSharedPreference.stringOrNull(key: String? = null) =
    delegateWithNull(
        key,
        setter = { prefKey, value ->
            putString(prefKey, value)
        },
        getter = {
            getString(it, null)
        },
    )
