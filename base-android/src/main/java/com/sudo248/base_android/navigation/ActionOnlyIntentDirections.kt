package com.sudo248.base_android.navigation

import android.content.Intent
import kotlin.reflect.KClass


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 11:53 - 18/02/2023
 */
internal data class ActionOnlyIntentDirections(override val destination: KClass<*>) :
    IntentDirections {
    override val intent: Intent = Intent()

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        val that = other as ActionOnlyIntentDirections
        return destination == that.destination
    }

    override fun hashCode(): Int {
        var result = 1
        result = 31 * result + destination.hashCode()
        return result
    }

    override fun toString(): String {
        return "ActionOnlyIntentDirections(destination=$destination)"
    }
}
