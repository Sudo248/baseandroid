package com.sudo248.base_android.navigation

import android.content.Intent
import kotlin.reflect.KClass


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 11:50 - 18/02/2023
 */

/**
 * An interface that describes a navigation operation:
 */
interface IntentDirections {
    val destination: KClass<*>
    val intent: Intent
}