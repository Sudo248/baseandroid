package com.sudo248.base_android.navigation

import android.os.Bundle


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 16:46 - 16/02/2023
 */
interface Navigator<Directions> {
    fun navigateTo(directions: Directions)
    fun navigateOff(directions: Directions)
    fun navigateOffAll(directions: Directions)
    fun navigateForResult(directions: Directions, key: String, result: ResultCallback)
    fun back(key: String? = null, data: Bundle? = null)
}
