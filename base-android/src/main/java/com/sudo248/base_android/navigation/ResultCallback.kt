package com.sudo248.base_android.navigation

import android.os.Bundle


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 15:48 - 18/02/2023
 */
interface ResultCallback {
    fun onResult(key: String, data: Bundle?)
}