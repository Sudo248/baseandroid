package com.sudo248.base_android.ui.binding

import android.view.View
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.gone
import com.sudo248.base_android.ktx.visible

@BindingAdapter("bindUiStateLoading")
fun bindUiStateLoading(view: View, state: SingleEvent<UiState>) {
    state.getValueIfNotHandled()?.let {
        view.isVisible = it == UiState.LOADING
    } ?: view.gone()
}

@BindingAdapter("bindUiStateSuccess")
fun bindUiStateSuccess(view: View, state: SingleEvent<UiState>) {
    state.getValueIfNotHandled()?.let {
        view.isVisible = it == UiState.SUCCESS
    } ?: view.gone()
}