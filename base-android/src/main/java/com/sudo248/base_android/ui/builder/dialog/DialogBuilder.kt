package com.sudo248.base_android.ui.builder.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.sudo248.base_android.R
import com.sudo248.base_android.ktx.showWithTransparentBackground
import com.sudo248.base_android.ktx.visible

class DialogBuilder private constructor(private val context: Context) {
    companion object {
        fun with(context: Context): DialogBuilder {
            return DialogBuilder(context)
        }
    }

    private val properties = DialogBuilderProperties()

    fun setTitle(title: String) = apply {
        properties.title = title
    }

    fun setDescription(description: String) = apply {
        properties.description = description
    }

    fun setTextConfirm(confirm: String) = apply {
        properties.confirm = confirm
    }

    fun setTextPositive(positive: String) = apply {
        properties.positive = positive
    }

    fun setTextNegative(negative: String) = apply {
        properties.negative = negative
    }

    fun setColorTitleResource(@ColorRes colorId: Int) = apply {
        properties.textColorTitle = ContextCompat.getColor(context, colorId)
    }

    fun setColorTitle(@ColorInt color: Int) = apply {
        properties.textColorTitle = color
    }

    fun setColorDescriptionResource(@ColorRes colorId: Int) = apply {
        properties.textColorDescription = ContextCompat.getColor(context, colorId)
    }

    fun setColorDescription(@ColorInt color: Int) = apply {
        properties.textColorDescription = color
    }

    fun setColorTextConfirmResource(@ColorRes colorId: Int) = apply {
        properties.textConfirmColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorTextConfirm(@ColorInt color: Int) = apply {
        properties.textConfirmColor = color
    }

    fun setColorBackgroundConfirmResource(@ColorRes colorId: Int) = apply {
        properties.backgroundConfirmColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorBackgroundConfirm(@ColorInt color: Int) = apply {
        properties.backgroundConfirmColor = color
    }

    fun setColorTextPositiveResource(@ColorRes colorId: Int) = apply {
        properties.textPositiveColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorTextPositive(@ColorInt color: Int) = apply {
        properties.textPositiveColor = color
    }

    fun setColorBackgroundPositiveResource(@ColorRes colorId: Int) = apply {
        properties.backgroundPositiveColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorBackgroundPositive(@ColorInt color: Int) = apply {
        properties.backgroundPositiveColor = color
    }

    fun setColorTextNegativeResource(@ColorRes colorId: Int) = apply {
        properties.textNegativeColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorTextNegative(@ColorInt color: Int) = apply {
        properties.textNegativeColor = color
    }

    fun setColorBackgroundNegativeResource(@ColorRes colorId: Int) = apply {
        properties.backgroundNegativeColor = ContextCompat.getColor(context, colorId)
    }

    fun setColorBackgroundNegative(@ColorInt color: Int) = apply {
        properties.backgroundNegativeColor = color
    }

    fun setImageResource(@DrawableRes drawableId: Int) = apply {
        properties.imageResource = drawableId
    }

    fun setOnClickPositiveListener(onPositive: (View) -> Unit) = apply {
        properties.onPositive = onPositive
    }

    fun setOnClickNegativeListener(onNegative: (View) -> Unit) = apply {
        properties.onNegative = onNegative
    }

    fun setOnClickConfirmListener(onConfirm: (View) -> Unit) = apply {
        properties.onConfirm = onConfirm
    }

    fun build(): Dialog {
        return if (needBuildConfirmDialog()) {
            buildConfirmDialog()
        } else {
            buildInformationDialog()
        }

    }

    fun buildAndShow() {
        build().showWithTransparentBackground()
    }

    private fun needBuildConfirmDialog(): Boolean {
        return properties.run {
            positive != null || negative != null || textPositiveColor != null ||
                    textNegativeColor != null || backgroundNegativeColor != null ||
                    backgroundPositiveColor != null || onPositive != null || onNegative != null
        }
    }

    private fun needBuildInformationDialog(): Boolean {
        return properties.run {
            confirm != null || textConfirmColor != null ||
                    backgroundConfirmColor != null || onConfirm != null
        }
    }

    @SuppressLint("InflateParams")
    fun buildConfirmDialog(): Dialog {
        return properties.run {
            checkNotNull(title) { "Title is required not null" }
            val dialog = Dialog(context)
            val view = LayoutInflater.from(context).inflate(R.layout.base_android_dialog_confirm, null, false)
            dialog.setContentView(view)
            view.run {
                imageResource?.let {
                    findViewById<ImageView>(R.id.imgDialog).apply {
                        visible()
                        setImageResource(it)
                    }
                }

                findViewById<TextView>(R.id.txtTitle).apply {
                    text = title
                    textColorTitle?.let { color ->
                        setTextColor(color)
                    }
                }

                description?.let {
                    findViewById<TextView>(R.id.txtDescription).apply {
                        text = it
                        textColorDescription?.let { color ->
                            setTextColor(color)
                        }
                    }
                }

                findViewById<TextView>(R.id.txtPositive).apply {
                    text = positive ?: context.getString(R.string.base_android_ok)
                    textPositiveColor?.let { color ->
                        setTextColor(color)
                    }
                    backgroundPositiveColor?.let {
                        backgroundTintList = ColorStateList.valueOf(it)
                    }
                    setOnClickListener {
                        dialog.dismiss()
                        onPositive?.invoke(it)
                    }
                }

                findViewById<TextView>(R.id.txtNegative).apply {
                    text = negative ?: context.getString(R.string.base_android_cancel)
                    textNegativeColor?.let { color ->
                        setTextColor(color)
                    }
                    backgroundNegativeColor?.let {
                        backgroundTintList = ColorStateList.valueOf(it)
                    }
                    setOnClickListener {
                        dialog.dismiss()
                        onNegative?.invoke(it)
                    }
                }
            }
            dialog
        }
    }

    fun buildConfirmDialogAndShow() {
        buildConfirmDialog().showWithTransparentBackground()
    }

    @SuppressLint("InflateParams")
    fun buildInformationDialog(): Dialog {
        return properties.run {
            checkNotNull(title) { "Title require not null" }
            val dialog = Dialog(context)
            val view = LayoutInflater.from(context).inflate(R.layout.base_android_dialog_infomation, null, false)
            dialog.setContentView(view)
            view.run {
                imageResource?.let {
                    findViewById<ImageView>(R.id.imgDialog).apply {
                        visible()
                        setImageResource(it)
                    }
                }

                findViewById<TextView>(R.id.txtTitle).apply {
                    text = title
                    textColorTitle?.let { color ->
                        setTextColor(color)
                    }
                }

                findViewById<TextView>(R.id.txtDescription).apply {
                    text = description
                    textColorDescription?.let { color ->
                        setTextColor(color)
                    }
                }

                findViewById<TextView>(R.id.txtConfirm).apply {
                    text = confirm ?: context.getString(R.string.base_android_ok)
                    textConfirmColor?.let { color ->
                        setTextColor(color)
                    }
                    backgroundConfirmColor?.let { color ->
                        backgroundTintList = ColorStateList.valueOf(color)
                    }
                    setOnClickListener {
                        dialog.dismiss()
                        onConfirm?.invoke(it)
                    }
                }
            }
            dialog
        }
    }

    fun buildInformationDialogAndShow() {
        buildInformationDialog().showWithTransparentBackground()
    }

}