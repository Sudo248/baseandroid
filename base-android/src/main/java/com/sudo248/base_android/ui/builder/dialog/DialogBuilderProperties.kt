package com.sudo248.base_android.ui.builder.dialog

import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes

internal data class DialogBuilderProperties(
    var title: String? = null,
    var description: String? = null,
    var confirm: String? = null,
    var positive: String? = null,
    var negative: String? = null,

    @ColorInt
    var textColorTitle: Int? = null,

    @ColorInt
    var textColorDescription: Int? = null,

    @ColorInt
    var textConfirmColor: Int? = null,

    @ColorInt
    var backgroundConfirmColor: Int? = null,

    @ColorInt
    var textPositiveColor: Int? = null,

    @ColorInt
    var backgroundPositiveColor: Int? = null,

    @ColorInt
    var textNegativeColor: Int? = null,

    @ColorInt
    var backgroundNegativeColor: Int? = null,

    @DrawableRes
    var imageResource: Int? = null,

    var onPositive: ((View) -> Unit)? = null,

    var onNegative: ((View) -> Unit)? = null,

    var onConfirm: ((View) -> Unit)? = null,
)
