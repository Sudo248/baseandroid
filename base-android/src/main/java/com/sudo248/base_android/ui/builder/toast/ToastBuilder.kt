package com.sudo248.base_android.ui.builder.toast

import android.content.Context
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes

class ToastBuilder private constructor(private val toast: Toast) {

    companion object {
        fun with(context: Context): ToastBuilder {
            return ToastBuilder(Toast(context))
        }

        fun with(toast: Toast): ToastBuilder {
            return ToastBuilder(toast)
        }
    }

    private val properties = ToastBuilderProperties()

    fun setDuration(duration: Int): ToastBuilder = apply {
        properties.duration = duration
    }

    fun isSingle(isSingle: Boolean): ToastBuilder = apply {
        properties.isSingle = isSingle
    }

    @RequiresApi(Build.VERSION_CODES.R)
    fun addCallback(callback: Toast.Callback): ToastBuilder = apply {
        toast.addCallback(callback)
    }

    fun setText(text: String): ToastBuilder = apply {
        toast.setText(text)
    }

    fun setText(@StringRes id: Int): ToastBuilder = apply {
        toast.setText(id)
    }

    fun setGravity(gravity: Int): ToastBuilder = apply {
        properties.gravity = gravity
    }

    fun setXOffset(xOffset: Int): ToastBuilder = apply {
        properties.xOffset = xOffset
    }

    fun setYOffSet(yOffset: Int): ToastBuilder = apply {
        properties.yOffset = yOffset
    }

    fun setHorizontalMargin(horizontalMargin: Float): ToastBuilder = apply {
        properties.horizontalMargin = horizontalMargin
    }

    fun setVerticalMargin(verticalMargin: Float): ToastBuilder = apply {
        properties.verticalMargin = verticalMargin
    }

    fun build(): Toast {
        return properties.run {
            toast.duration = duration ?: Toast.LENGTH_SHORT
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                toast.setGravity(
                    gravity ?: toast.gravity,
                    xOffset ?: toast.xOffset,
                    yOffset ?: toast.yOffset
                )
                toast.setMargin(
                    horizontalMargin ?: toast.horizontalMargin,
                    verticalMargin ?: toast.verticalMargin
                )
            }
            if (isSingle) toast.cancel()
            toast
        }
    }

    fun buildAndShow() = build().show()
}