package com.sudo248.base_android.ui.builder.toast

internal data class ToastBuilderProperties(
    var horizontalMargin: Float? = null,
    var verticalMargin: Float? = null,
    var gravity: Int? = null,
    var xOffset: Int? = null,
    var yOffset: Int? = null,
    var duration: Int? = null,
    var isSingle: Boolean = false,
)
