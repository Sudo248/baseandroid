package com.sudo248.base_android.ui.widget

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import com.sudo248.base_android.R
import com.sudo248.base_android.utils.ColorUtils

class SudoDefaultLoadingView
@JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    init {
        val width =
            attrs?.getAttributeIntValue(android.R.attr.layout_width, LayoutParams.MATCH_PARENT)
                ?: LayoutParams.MATCH_PARENT
        val height =
            attrs?.getAttributeIntValue(android.R.attr.layout_height, LayoutParams.MATCH_PARENT)
                ?: LayoutParams.MATCH_PARENT
        layoutParams = LayoutParams(width, height)

        isClickable = true
        isFocusable = true

        val defaultBackgroundColor = ColorUtils.fromArgb(126, 150, 150, 150)
        val backgroundColor =
            attrs?.getAttributeIntValue(android.R.attr.backgroundTint, defaultBackgroundColor)
                ?: defaultBackgroundColor
        setBackgroundColor(backgroundColor)

        attrs?.getAttributeResourceValue(android.R.attr.background, 0)?.let {
            try {
                background = ContextCompat.getDrawable(context, it)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val attributes =
            context.theme.obtainStyledAttributes(attrs, R.styleable.base_android_SudoDefaultLoadingView, 0, 0)

        val backgroundLoadingView = FrameLayout(context)
        backgroundLoadingView.layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        ).apply {
            gravity = Gravity.CENTER
        }
        backgroundLoadingView.setPadding(
            attributes.getInt(
                R.styleable.base_android_SudoDefaultLoadingView_backgroundLoadingPadding,
                18
            )
        )
        backgroundLoadingView.background = GradientDrawable().apply {
            cornerRadius = attributes.getFloat(
                R.styleable.base_android_SudoDefaultLoadingView_backgroundLoadingConnerRadius,
                32f
            )
        }
        backgroundLoadingView.backgroundTintList =
            ColorStateList.valueOf(
                attributes.getColor(
                    R.styleable.base_android_SudoDefaultLoadingView_backgroundLoadingColor,
                    ColorUtils.fromArgb(255, 50, 50, 50)
                )
            )
        backgroundLoadingView.addView(ProgressBar(context).apply {
            layoutParams = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            )
            indeterminateTintList = ColorStateList.valueOf(
                attributes.getColor(
                    R.styleable.base_android_SudoDefaultLoadingView_indeterminateTint,
                    Color.WHITE
                )
            )
        })
        addView(backgroundLoadingView)
        elevation = attrs?.getAttributeFloatValue(android.R.attr.elevation, 100f) ?: 100f
    }
}