package com.sudo248.base_android.utils

import android.content.Context
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreference
import com.sudo248.base_android.data.local.shared_preference.factory.SudoSharedPreferenceFactory

/**
 * # Created by
 * @author **Sudo248**
 * @since 14:13 - 14/08/2022
 */
object AppSharedPreference {
    private var appPref: SudoSharedPreference? = null

    private fun getApplicationName(applicationContext: Context): String {
        val applicationInfo = applicationContext.applicationInfo
        val nameId = applicationInfo.labelRes
        val appName = if (nameId == 0) {
            applicationInfo.nonLocalizedLabel.toString()
        } else {
            applicationContext.getString(nameId)
        }
        return "[Application] $appName"
    }

    fun create(applicationContext: Context, namePref: String? = null): SudoSharedPreference {
        val name = namePref ?: getApplicationName(applicationContext)
        appPref = SudoSharedPreferenceFactory.create(applicationContext, name)
        return appPref!!
    }

    fun get(): SudoSharedPreference {
        return checkNotNull(appPref) {
            "Must call AppSharedPreference.create(context) first"
        }
    }

    val I: SudoSharedPreference
        get() = get()
}