package com.sudo248.base_android.utils

import android.content.Context
import androidx.annotation.StringRes
import com.sudo248.base_android.ui.builder.toast.ToastBuilder

object AppToast {

    private var toastBuilder: ToastBuilder? = null

    fun create(context: Context) {
        toastBuilder = ToastBuilder.with(context)
    }

    fun builder(): ToastBuilder {
        return checkNotNull(toastBuilder) {
            "Must call AppToast.create(context) first"
        }
    }

    fun setText(text: String): AppToast {
        builder().setText(text)
        return this
    }

    fun setText(@StringRes id: Int): AppToast {
        builder().setText(id)
        return this
    }

    fun isSingle(isSingle: Boolean): AppToast {
        builder().isSingle(isSingle)
        return this
    }

    fun show(isSingle: Boolean = true) = builder().isSingle(isSingle).buildAndShow()

    fun show(text: String, isSingle: Boolean = true) =
        builder().setText(text).isSingle(isSingle).buildAndShow()

    fun show(@StringRes id: Int, isSingle: Boolean = true) =
        builder().setText(id).isSingle(isSingle).buildAndShow()

    // for databinding
    fun show(text: String) = show(text, isSingle = true)

    fun show(@StringRes id: Int) = show(id, isSingle = true)

}