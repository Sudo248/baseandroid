package com.sudo248.base_android.utils

import android.graphics.Color

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

object ColorUtils {
    fun fromArgb(alpha: Int = 255, red: Int = 255, green: Int = 255, blue: Int = 255): Int {
        return (alpha and 0xff shl 24) or (red and 0xff shl 16) or (green and 0xff shl 8) or (blue and 0xff)
    }

    fun getAlphaFrom(color: Int): Int {
        return (color shr 24) and 0xff
    }

    fun getRedFrom(color: Int): Int {
        return (color shr 16) and 0xff
    }

    fun getGreenFrom(color: Int): Int {
        return (color shr 8) and 0xff
    }

    fun getBlueFrom(color: Int): Int {
        return color and 0xff
    }

    fun parseColor(colorString: String): Int = Color.parseColor(colorString)

    fun toHexString(color: Int): String {
        return String.format("#%06X", 0xFFFFFF and color)
    }

    fun toHexStringWithAlpha(color: Long): String {
        return String.format("#%08X", 0xFFFFFFFF and color)
    }

}