package com.sudo248.base_android.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.widget.addTextChangedListener
import com.sudo248.base_android.R
import com.sudo248.base_android.ktx.invisible
import com.sudo248.base_android.ktx.showWithTransparentBackground
import com.sudo248.base_android.ktx.visible
import com.sudo248.base_android.ui.builder.dialog.DialogBuilder

/**
 * **Created by**
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

object DialogUtils {

    @SuppressLint("InflateParams")
    fun showDialog(
        context: Context,
        title: String,
        description: String,
        @ColorRes textColorTitle: Int? = null,
        @ColorRes textColorDescription: Int? = null,
        confirm: String? = null,
        @ColorRes textConfirmColor: Int? = null,
        @ColorRes backgroundConfirmColor: Int? = null,
        @DrawableRes imageResource: Int? = null,
        onClickConfirm: (() -> Unit)? = null,
    ): Dialog {
        val dialog = DialogBuilder.with(context).run {
            setTitle(title)
            setDescription(description)
            textColorTitle?.let { setColorTitleResource(it) }
            textColorDescription?.let { setColorDescriptionResource(it) }
            confirm?.let { setTextConfirm(it) }
            textConfirmColor?.let { setColorTextConfirmResource(it) }
            backgroundConfirmColor?.let { setColorBackgroundConfirmResource(it) }
            imageResource?.let { setImageResource(it) }
            setOnClickConfirmListener { onClickConfirm?.invoke() }
        }.buildInformationDialog()
        dialog.showWithTransparentBackground()
        return dialog
    }

    @SuppressLint("InflateParams")
    fun showConfirmDialog(
        context: Context,
        title: String,
        description: String,
        @ColorRes textColorTitle: Int? = null,
        @ColorRes textColorDescription: Int? = null,
        positive: String? = null,
        @ColorRes textPositiveColor: Int? = null,
        negative: String? = null,
        @ColorRes textNegativeColor: Int? = null,
        @DrawableRes imageResource: Int? = null,
        onPositive: (() -> Unit)? = null,
        onNegative: (() -> Unit)? = null,
    ): Dialog {
        val dialog = DialogBuilder.with(context).run {
            setTitle(title)
            setDescription(description)
            textColorTitle?.let { setColorTitleResource(it) }
            textColorDescription?.let { setColorDescriptionResource(it) }
            positive?.let { setTextPositive(it) }
            textPositiveColor?.let { setColorTextPositiveResource(it) }
            negative?.let { setTextNegative(it) }
            textNegativeColor?.let { setColorTextNegativeResource(it) }
            imageResource?.let { setImageResource(it) }
            setOnClickPositiveListener {
                onPositive?.invoke()
            }
            setOnClickNegativeListener {
                onNegative?.invoke()
            }
        }.buildConfirmDialog()

        dialog.showWithTransparentBackground()
        return dialog
    }

    @SuppressLint("InflateParams")
    fun showInputDialog(
        context: Context,
        title: String,
        onSubmit: (String) -> Unit,
        hint: String? = null,
        error: String? = null,
        submit: String? = null,
        cancel: String? = null,
        initValue: String? = null,
        maxLines: Int? = null,
        lines: Int? = null,
        onCancel: (() -> Unit)? = null,
    ): Dialog {
        val dialog = Dialog(context)
        val view = LayoutInflater.from(context).inflate(R.layout.base_android_dialog_input, null, false)
        dialog.setContentView(view)

        view.apply {
            findViewById<TextView>(R.id.txtTitle).text = title
            val edtInput = findViewById<EditText>(R.id.edtInput)
            hint?.let {
                edtInput.hint = hint
            }
            val textError = findViewById<TextView>(R.id.txtError)
            error?.let {
                textError.text = it
            }
            findViewById<TextView>(R.id.txtPositive).apply {
                text = submit ?: context.getString(R.string.base_android_submit)
                setOnClickListener {
                    dialog.dismiss()
                    onSubmit(edtInput.text.toString())
                }
            }

            findViewById<TextView>(R.id.txtNegative).apply {
                text = cancel ?: context.getString(R.string.base_android_cancel)
                setOnClickListener {
                    dialog.dismiss()
                    onCancel?.invoke()
                }
            }
            initValue?.let {
                edtInput.setText(initValue)
            }

            maxLines?.let {
                edtInput.maxLines = it
            }

            lines?.let {
                edtInput.setLines(it)
            }

            edtInput.addTextChangedListener {
                if (it.isNullOrEmpty()) {
                    textError.visible()
                } else {
                    textError.invisible()
                }
            }
        }

        dialog.showWithTransparentBackground()
        return dialog
    }
}