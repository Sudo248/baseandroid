package com.sudo248.base_android.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.sudo248.base_android.connectivity.ConnectivityObserver
import com.sudo248.base_android.connectivity.NetworkConnectivityObserver
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach


/**
 * **Created by**
 *
 * @author *Sudo248*
 * @since 10:19 - 25/02/2023
 */
object NetworkUtils {
    private var applicationContext: Context? = null

    private var status: ConnectivityObserver.Status = ConnectivityObserver.Status.UNAVAILABLE

    internal fun installIn(context: Context) {
        applicationContext = context
    }

    private fun getNetWorkConnectObserver(): ConnectivityObserver {
        val context = requireNotNull(applicationContext) { "Must call installIn(context) first" }
        return NetworkConnectivityObserver(context)
    }

    fun status() = status

    suspend fun observer(onChange: ((ConnectivityObserver.Status) -> Unit)? = null): Unit = getNetWorkConnectObserver().observer().onEach {
        status = it
        onChange?.invoke(status)
    }.collect()

    fun hasNetwork(): Boolean = status.hasNetwork()

    fun isNetworkConnected(): Boolean {
        val context = requireNotNull(applicationContext) { "Must call installIn(context) first" }
        val connectivityManager = context.getSystemService(ConnectivityManager::class.java)
        val network = connectivityManager.activeNetwork
        val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
        return networkCapabilities != null &&
                (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))
    }

    fun cleared() {
        applicationContext = null
    }
}