package com.sudo248.base_android.utils

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.core.view.setPadding
import com.sudo248.base_android.ui.widget.SudoDefaultLoadingView

/**
 * ## Created by
 * @author **Sudo248**
 * @since 00:00 - 15/08/2022
 */

internal object ViewUtils {

    const val LOADING_DEFAULT = 24082001

    fun getDefaultLoadingView(context: Context): FrameLayout {
        return SudoDefaultLoadingView(context)
    }
}