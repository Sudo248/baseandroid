package com.sudo.base_android.bus

import com.sudo.base_android.model.User
import com.sudo248.base_android.core.bus.HandleSubscriber
import com.sudo248.base_android.core.bus.Subscriber
import com.sudo248.base_android.core.bus.SudoBus
import com.sudo248.base_android.core.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class SudoBusTest {
    lateinit var bus: SudoBus
    @OptIn(ExperimentalCoroutinesApi::class)
    val testDispatcher = StandardTestDispatcher(name = "TestMain")

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun `Init before each test`() {
        bus = SudoBus.getInstance()
        Dispatchers.setMain(testDispatcher)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Test publish and subscribe`() = runTest {
        val user = User("12345")
        launch {
            val receiverUser = suspendCoroutine {
                bus.subscribe("TestEvent", object : Subscriber<User>() {
                    override suspend fun onReceive(event: User) {
                        it.resume(event)
                    }
                })
            }
            Assert.assertEquals(user, receiverUser)
        }
        delay(10)
        bus.publish("TestEvent", user)
        advanceUntilIdle()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Test handle success event`() = runTest {
        val user = User("12345")

        bus.subscribeHandler("TestHandlerSuccessEvent", object : HandleSubscriber<Unit, User>() {
            override suspend fun handle(event: Unit): DataState<User, Exception> {
                val result = DataState.Success(user)
                println("Sudoo: result = $result")
                return result
            }
        })
        delay(10)
        val dataUser = bus.request<User>("TestHandlerSuccessEvent", Unit)
        advanceUntilIdle()
        Assert.assertTrue(dataUser.isSuccess)
        Assert.assertFalse(dataUser.isLoading)
        Assert.assertFalse(dataUser.isError)
        Assert.assertEquals(user, dataUser.requireData())
        Assert.assertNotEquals(User("123"), dataUser.requireData())
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Test handle error event`() = runTest {
        bus.subscribeHandler("TestHandlerErrorEvent", object : HandleSubscriber<Unit, User>() {
            override suspend fun handle(event: Unit): DataState<User, Exception> {
                return DataState.Error(Exception("Error"))
            }
        })
        delay(10)
        val dataUser = bus.request<User>("TestHandlerErrorEvent", Unit)
        advanceUntilIdle()
        Assert.assertTrue(dataUser.isError)
        Assert.assertFalse(dataUser.isLoading)
        Assert.assertFalse(dataUser.isSuccess)
        Assert.assertEquals("Error", dataUser.error().message)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Test handle success event callback`() = runTest{
        val user = User("12345")
        bus.subscribeHandler("TestHandlerSuccessEventCallback", object : HandleSubscriber<Unit, User>() {
            override suspend fun handle(event: Unit): DataState<User, Exception> {
                return DataState.Success(user)
            }
        })
        delay(10)
        launch {
            val receiveUser = suspendCoroutine { continuation ->
                bus.request<User>("TestHandlerSuccessEventCallback", Unit) {
                    continuation.resume(it)
                }
            }
            Assert.assertTrue(receiveUser.isSuccess)
            Assert.assertFalse(receiveUser.isLoading)
            Assert.assertFalse(receiveUser.isError)
            Assert.assertEquals(user, receiveUser.requireData())
            Assert.assertNotEquals(User("123"), receiveUser.requireData())
        }
        advanceUntilIdle()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `Test handle error event callback`() = runTest{
        bus.subscribeHandler("TestHandlerErrorEventCallback", object : HandleSubscriber<Unit, User>() {
            override suspend fun handle(event: Unit): DataState<User, Exception> {
                return DataState.Error(Exception("Error"))
            }
        })
        delay(10)
        launch {
            val receiveUser = suspendCoroutine { continuation ->
                bus.request<User>("TestHandlerErrorEventCallback", Unit) {
                    continuation.resume(it)
                }
            }
            Assert.assertTrue(receiveUser.isError)
            Assert.assertFalse(receiveUser.isLoading)
            Assert.assertFalse(receiveUser.isSuccess)
            Assert.assertEquals("Error", receiveUser.error().message)
        }
        advanceUntilIdle()
    }

}