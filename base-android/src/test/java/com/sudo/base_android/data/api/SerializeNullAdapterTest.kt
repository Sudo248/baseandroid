package com.sudo.base_android.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sudo.base_android.model.User
import com.sudo248.base_android.data.gson.GsonSerializeNullAdapterFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SerializeNullAdapterTest {
    val user = User("12345")
    lateinit var gson: Gson

    @Before
    fun `init gson`() {
        gson = GsonBuilder()
            .registerTypeAdapterFactory(GsonSerializeNullAdapterFactory())
            .create()
    }

    @Test
    fun `test serialize user`() {
        Assert.assertEquals(user.toJson(), gson.toJson(user))
    }

}