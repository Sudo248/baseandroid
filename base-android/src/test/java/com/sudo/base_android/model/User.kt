package com.sudo.base_android.model

import com.google.gson.annotations.SerializedName
import com.sudo248.base_android_annotation.apiservice.SerializeNull

data class User(
    val userId: String,
    @SerializedName("user_name")
    @SerializeNull
    val userName: String? = null,
    @SerializeNull
    val email: String? = null,
    val address: String? = null
) {
    fun toJson(): String {
        return buildString {
            append("{")
            append("\"userId\":\"$userId\",")
            if (userName != null) {
                append("\"user_name\":\"$userName\"")
            } else {
                append("\"user_name\":null,")
            }
            if (email != null) {
                append("\"email\":\"$email\"")
            } else {
                append("\"email\":null")
            }
            append("}")
        }

    }
}
