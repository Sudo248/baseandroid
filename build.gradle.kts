// Top-level build file where you can add configuration options common to all sub-projects/modules.
import dependencies.BaseArtifacts
import ktx.getLocalProperties
import ktx.getProjectProperty
import ktx.getSystemEnv
import version.Versions
import model.ProjectProperties
import model.LocalProperties

buildscript {
    dependencies {
        classpath(RootProject.Dependencies.hiltGradle)
        classpath(RootProject.Dependencies.nexusPublishPlugin)
        classpath(RootProject.Dependencies.navigationSafeArgs)
    }
    repositories {
        google()
    }
}

plugins {
    kotlin(RootProject.Plugins.kotlinSerialization) version RootProject.Versions.kotlinSerialization apply false
    id(RootProject.Plugins.nexusPublishPlugin) version RootProject.Versions.nexusPublishPlugin
}

ext {
    set(BaseArtifacts.baseAndroid, Versions.Base.android)
    set(BaseArtifacts.baseAndroidAnnotation, Versions.Base.annotation)
    set(BaseArtifacts.baseAndroidProcessor, Versions.Base.processor)
}

nexusPublishing {
    repositories {
        sonatype {
            stagingProfileId.set(
                getLocalProperties(
                    LocalProperties.SONATYPE_STAGING_PROFILE_ID.key,
                    getSystemEnv("SONATYPE_STAGING_PROFILE_ID")
                )
            )
            nexusUrl.set(uri(getProjectProperty(ProjectProperties.PUBLISH_SERVICE_URL)))
            snapshotRepositoryUrl.set(uri(getProjectProperty(ProjectProperties.PUBLISH_CONTENT_URL)))

            username.set(
                getLocalProperties(
                    LocalProperties.OSSRH_USERNAME.key,
                    getSystemEnv("OSSRH_USERNAME")
                )
            )

            password.set(
                getLocalProperties(
                    LocalProperties.OSSRH_PASSWORD.key,
                    getSystemEnv("OSSRH_PASSWORD")
                )
            )
        }
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}